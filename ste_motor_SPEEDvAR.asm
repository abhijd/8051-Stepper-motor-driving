 MOV P2,0C0H ; P2.7 and P2.6 are set as input
 MOV P3,0FFH ; P3 is set as input
 MOV R2,#33H ; sequence for 2 phase excitation
CHECK:
 MOV P1,00H  ; P1 is cleared
 JB P2.7,ACW ; If P2.7 is set Jumps to ACW
 JB P2.6,CW  ; If P2.6 is set Jumps to CW
 SJMP CHECK  ; The loop runs indefinately to until any input at p2.7 and p2.6

ACW:
 MOV A, R2          ; The 2 phase excitation sequence is copied to A
RotateCW:
 MOV P1, A          ; The sequence is sent to the motor via P1
 ACALL DELAY        ; Delay block is called
 JNB P2.7,CHECK     ; The p2.7 is checked
 RR A               ; The the bits are shifted 1 bit to the right
 SJMP RotateCW      ; The loop continues 
 
CW:
 MOV A, R2          ; The 2 phase excitation sequence is copied to A
RotateACW:
 MOV P1, A          ; The sequence is sent to the motor via P1
 ACALL DELAY        ; Delay block is called
 JNB P2.6,CHECK     ; The p2.7 is checked
 RL A               ; The the bits are shifted 1 bit to the right
 SJMP RotateACW     ; The loop continues 
 
 
DELAY: MOV R4,P3       ;The R4 is copied with value given by the user 
LOOP2: MOV R5, #03FH   ; R5 is copied with hex value 3F 
LOOP1: DJNZ R5, LOOP1  ; R5 decreased until it is zero
 DJNZ R4, LOOP2        ;R4 decreased and jumps to Loop2
 RET
 END