# Stepper motor driving using 8051

A simple project to drive a stepper motor using 8051 microprocessor

## Simulating

To simulate, you must install all the software listed in the prerequisite section and then following the "How to simulate" section 

### Prerequisites

 * Proteus Design Suite


### How-to-simulate
 
 * Clone the repo in your local directory
 * Assemble the ste_motor_SPEEDvAR.asm file into HEX file using any suitable assembler of your choice
 * Start Proteus Design Suite
 * In Proteus, open "DIFF_4.DSN"  
 * When the design opens, double-click on the microprocessor that opens the "edit component" pop-up
 * From the edit component, add the location of the hex file in the program file field and click OK.
 * Press the play button from the bottom left menu. Voila!!
 * Close or open the switches to control the speed(top four switch) and direction of rotation (bottom two)

## Changing the program code

  * To change the program code, you may use any text editor to change the assembly code  
  * Use any assembly to hex converter program to convert the assembly code to hex format. (you may download one from www.microchip.com if you do not want to ask google)
  * Once changed and recompiled into hex file, load it again in the microprocessor as instructed in "How-to-simulate" section and run

## Hardware Implementation 

  * You must select the model of motor and its controller according to the frame and weight of installation. 
  * A simple voltage regulator circuit should be used to power the microprocessor
  * A crystal circuit must also be used with the 8051 MPU since it doesn't have a builtin one
  * Before printing the circuit in board, experimentally make sure the hardware implementation work because the real world is not always similar to simulation world. We had to do several tweaks to make it work in the real world.  
  * The hex file must me burned (not literally) in the microprocessor using some universal programmer that supports it.
  
## Program flow

![Program flow](Software_flow.png "Program Flow")

 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required to do the project. 
